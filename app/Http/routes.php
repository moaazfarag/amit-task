<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::get('/login',["uses"=>'AuthController@postLogin',"as"=>"login"] );
Route::post('/getusername',["uses"=>'Auth\AuthController@getUsername',"as"=>"getUsername"] );
Route::post('/getemail',["uses"=>'Auth\AuthController@getEmail',"as"=>"getEmail"] );
Route::group(['middleware' => 'web'], function () {
    // Authentication Routes...
    $this->get('login', 'Auth\AuthController@showLoginForm');
    $this->post('login', 'Auth\AuthController@login');
    $this->get('logout', 'Auth\AuthController@logout');

    // Registration Routes...
    $this->get('signup', 'Auth\AuthController@showRegistrationForm');
    $this->post('signup', 'Auth\AuthController@register');
    Route::get('/dashboard', 'HomeController@index');
});
//    abort(403, 'Unauthorized action.');


