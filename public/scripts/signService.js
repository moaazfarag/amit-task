
angular.module('signService', [])

    .factory('Sign', function($http) {
        return {
            getUsername: function(user) {//is username  taken
                return $http({
                    method:'POST',
                    url: '/getusername',
                    headers: { 'Content-Type' : 'application/json' },
                    data:   JSON.stringify(user)
                });
            }   ,
            getEmail: function(email) {//is email taken
                return $http({
                    method:'POST',
                    url: '/getemail',
                    headers: { 'Content-Type' : 'application/json' },
                    data:   JSON.stringify(email)
                });
            }
        }
    });