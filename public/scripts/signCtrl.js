
angular.module('signCtrl', [])

    .controller('signController', function($scope,Sign) {
        $scope.isValidUsername = function (){
            user =  $scope.user;
            Sign.getUsername(user).success(function (data) {
                    $scope.isUsernameTaken = data;
                })
            .error(function (data) {
                });
        }
        $scope.isValidEmail = function (){
            user =  $scope.user;
            Sign.getEmail(user).success(function (data) {
                    $scope.isEmailTaken = data;
                })
            .error(function (data) {
                });
        }
    });