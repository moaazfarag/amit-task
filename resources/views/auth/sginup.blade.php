@extends('layouts.app')

@section('content')
<div ng-app="taskApp" ng-controller="signController"  class="container">
    <div class="row">
@{{ isUsername  }}
@{{ moaaz.success }}
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Sign Up</div>
                <div class="panel-body">
                    <form name="form" class="form-horizontal" role="form" method="POST" action="{{ url('/signup') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input ng-model="user.name"   type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Username</label>

                            <div class="col-md-6">
                                <input  ng-change="isValidUsername()" ng-pattern="/^[a-zA-Z0-9]*$/" ng-model="user.username" required type="text" class="form-control" name="username" value="{{ old('username') }}">
                                <div ng-show="form.$submitted || form.username.$touched">
                                    <span class="help-block" ng-show="form.username.$error.required">Tell us your username.</span>
                                    <span class="help-block" ng-show="form.username.$valid && !isUsernameTaken.success"><i class="btn-success fa  fa-thumbs-o-up"></i></span>
                                    <span class="help-block" ng-show="form.username.$error.pattern">must be number or char.</span>
                                    <span class="alert-danger" ng-show="isUsernameTaken.success">this is taken user name</span>
                                </div>
                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input ng-change="isValidEmail()"  ng-model="user.email" required type="email" class="form-control" name="email" value="{{ old('email') }}">
                                <div ng-show="form.$submitted || form.email.$touched">
                                    <span class="help-block" ng-show="form.email.$error.required">Tell us your email.</span>
                                    <span class="help-block" ng-show="form.email.$valid && !isEmailTaken.success"><i class="btn-success fa  fa-thumbs-o-up"></i></span>
                                    <span class="help-block" ng-show="form.email.$error.email">This is not a valid email.</span>
                                    <span class="alert-danger" ng-show="isEmailTaken.success">this is taken user name</span>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Password

                            </label>
                            <div class="col-md-6">

                                <input ng-model="user.password" minlength="6" ng-minlength="6" required type="password" class="form-control" name="password">
                           <span ng-show="passLength">
                                <i class="btn-success fa  fa-thumbs-o-up"></i>
                            </span>
                                <span ng-hide="passLength">
                                    <span class="help-block">
                                        min 6 chr
                                    </span>
                            </span>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input minlength="6" ng-model="user.password_confirmation" required type="password" class="form-control" name="password_confirmation">
                                <div ng-show="form.$submitted || form.password_confirmation.$touched">
                                    <span class="help-block" ng-show="form.password_confirmation.$error.required">write your password again .</span>
                                    <span class="help-block" class="alert-warning" ng-if="user.password_confirmation != user.password">password not matched</span>
                                    <span class="help-block" ng-show="form.password_confirmation.$error.minlength">min 6 char</span>
                                </div>
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button ng-disabled="form.$invalid || user.password_confirmation != user.password ||  isEmailTaken.success || isUsernameTaken.success" type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
